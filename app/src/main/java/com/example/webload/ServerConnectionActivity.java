package com.example.webload;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.URISyntaxException;

public class ServerConnectionActivity extends AppCompatActivity {

    EditText ipText, portText;
    Button connectButton;
    Socket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_connection);


        ipText = findViewById(R.id.ipEditText);
        portText = findViewById(R.id.portEditText);
        connectButton = findViewById(R.id.connectButton);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    socket = IO.socket("http://" + ipText.getText().toString() + ":" + portText.getText().toString());
                    socket.connect();
                    socket.on("connected", new Emitter.Listener() {
                        @Override
                        public void call(Object... args) {
                            String data = (String) args[0];

                            Toast.makeText(ServerConnectionActivity.this, data, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
